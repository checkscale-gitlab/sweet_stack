import mongoose from 'mongoose';

import User from './user';

export function connectDb() {
    if(process.env.MONGO_URL) {
        return mongoose.connect(
            process.env.MONGO_URL,
            { 
                useNewUrlParser: true,
                useUnifiedTopology: true
            }
        );
    }
    else {
        throw new Error(
            'No MONGO_URL environment variable ' +
            'provided. Please provide one through the ' + 
            '.env file at the root or by setting the ' + 
            'variable in the environment');
    }
};

export default { User };